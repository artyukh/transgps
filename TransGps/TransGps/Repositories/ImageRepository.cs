﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TransGps.Helpers;
using TransGps.Models;

namespace TransGps.Repositories
{
    public class ImageRepository : RepositoryBase
    {
        private List<Banner> _bannerList;
        public List<Banner> BannerList => _bannerList ?? (_bannerList = GetBanners());

        private List<Banner> GetBanners()
        {
            var banners = new List<Banner>();

            try
            {
                using (var client = new HttpClient(new LoggingMessageHandler(null)))
                {
                    var response = client.GetAsync("https://pub.trans-gps.cv.ua/api/v1/ads/list").Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        banners = JsonConvert.DeserializeObject<List<Banner>>(result).Where(banner => banner.Categories == "TopA").ToList();
                    }
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    var message = x.Message;
                    System.Console.WriteLine($"Error: AggregateException at ImageRepository.GetBanners() - {message}");

                    var typeEx = x.GetType();
                    return true;
                });
            }

            return banners;
        }

        public async Task<bool> ClickOnBanner(int bannerId)
        {
            var request = new BannerRequestModel
            {
                Id = bannerId.ToString()
            };

            try
            {
                using (var client = new HttpClient(new LoggingMessageHandler(null)))
                {
                    var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
                    var response = await client.PostAsync("https://pub.trans-gps.cv.ua/api/v1/ads/click/", content);

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        return true;
                    }
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    var message = x.Message;
                    Console.WriteLine($"Error: AggregateException at ImageRepository.GetBanners() - {message}");
                    return false;
                });
            }

            return false;
        }
        
        public async Task<bool> ShowBanner(int bannerId)
        {
            var request = new BannerRequestModel
            {
                Id = bannerId.ToString()
            };

            try
            {
                using (var client = new HttpClient(new LoggingMessageHandler(null)))
                {
                    var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
                    var response = await client.PostAsync("https://pub.trans-gps.cv.ua/api/v1/ads/click/", content);

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        return true;
                    }
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    var message = x.Message;
                    Console.WriteLine($"Error: AggregateException at ImageRepository.GetBanners() - {message}");
                    return false;
                });
            }

            return false;
        }
    }
}