﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;

using TransGps.Models;
using TransGps.ViewModels;

namespace TransGps.Repositories
{
    class UnitRepository
    {
        public List<Unit> GetUnits(List<RouteViewModel> routes = null)
        {
            var units = new List<Unit>();
            var selectedRouteId = routes?.Select(r => r.Id).ToList() ?? new List<int>();

            using (var client = new HttpClient())
            {
                try
                {
                    var response = client.GetAsync("https://trans-gps.cv.ua/map/api/get_trackers").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    var trackers = JsonConvert.DeserializeObject<List<Tracker>>(result);

                    units = trackers.Where(t => t.online.GetValueOrDefault() == true && t.route_id > 0 && (selectedRouteId.Count == 0 || selectedRouteId.Contains(t.route_id)))
                                    .Select((t, i) => new Unit
                    {
                        Id = i,
                        Lattitude = t.lat.HasValue ? t.lat.Value : 0,
                        Longtitude = t.lng.HasValue ? t.lng.Value : 0,
                        Rotation = t.orientation.HasValue ? t.orientation.Value : 0,
                        UnitTypeId = t.bus_type_id.HasValue ? t.bus_type_id.Value : 0,
                        Speed = t.speed.HasValue ? t.speed.Value : 0,
                        RouteId = t.route_id,
                        UnitName = t.name
                    }).ToList();
                }
                catch (Exception e)
                {
                    System.Console.WriteLine($"Error: Exception at UnitRepository.GetUnits(List<RouteViewModel> routes = null) - {e.Message}");
                    if (e.InnerException != null)
                    {
                        System.Console.WriteLine($"InnerException - {e.InnerException.Message}");
                    }
                }
            }

            return units;
        }
    }

    class Tracker
    {
        public string name { get; set; }
        public double? lat { get; set; }
        public double? lng { get; set; }
        public double? speed { get; set; }
        public double? orientation { get; set; }
        public int route_id { get; set; }
        public string busNumber { get; set; }
        public bool? online { get; set; }
        public int? bus_type_id { get; set; }
    }
}
