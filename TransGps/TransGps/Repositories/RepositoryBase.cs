﻿using Xamarin.Forms;
using TransGps.Interfaces;

namespace TransGps.Repositories
{
    public class RepositoryBase
    {
        private string dbFileName = "transgps.db";

        protected string DbPath => DependencyService.Get<IPath>().GetDataBasePath(dbFileName);

        public bool SourceChanged { get; set; }

        public void InitializeDb()
        {
            using (var db = new ApplicationContext(DbPath))
            {
                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();
            }
        }
    }
}
