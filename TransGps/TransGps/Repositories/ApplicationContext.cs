﻿using Microsoft.EntityFrameworkCore;
using TransGps.Models;

namespace TransGps.Repositories
{
    class ApplicationContext : DbContext
    {
        private string _databasePath;

        public DbSet<Route> Routes { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<UnitType> UnitTypes { get; set; }

        public ApplicationContext(string databasePath)
        {
            _databasePath = databasePath;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"FileName={_databasePath}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RouteStation>().HasKey(k => new { k.RouteId, k.StationId });

            modelBuilder.Entity<RouteStation>().HasOne(rs => rs.Route).WithMany(s => s.RouteStations).HasForeignKey(rs => rs.RouteId);

            modelBuilder.Entity<RouteStation>().HasOne(rs => rs.Station).WithMany(r => r.RouteStations).HasForeignKey(rs => rs.StationId);
        }
    }
}
