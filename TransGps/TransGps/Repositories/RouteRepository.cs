﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;

using TransGps.Models;
using TransGps.ViewModels;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Newtonsoft.Json;

namespace TransGps.Repositories
{
    class RouteRepository : RepositoryBase
    {
        private List<RouteViewModel> _routes;
        public List<RouteViewModel> Routes => _routes ?? (_routes = GetRoutes());

        private List<RouteViewModel> GetRoutes()
        {
            var routes = new List<RouteViewModel>();

            try
            {
                using (var client = new HttpClient())
                {
                    var response = client.GetAsync("https://trans-gps.cv.ua/map/api/get_routes").Result;

                    if (response.IsSuccessStatusCode) 
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        var jsonRouteList = JsonConvert.DeserializeObject<List<JsonRoute>>(result);

                        routes = jsonRouteList.Where(r => r.name != "ET")
                                              .OrderBy(r => r.sort_apk)
                                              .Select(jsonRoute => new RouteViewModel
                                               {
                                                   Id = jsonRoute.id,
                                                   RouteColor = jsonRoute.colour,
                                                   RouteName = jsonRoute.name,
                                                   Cost = jsonRoute.price,
                                                   RouteType = (RouteType)jsonRoute.bus_type_id
                                               }).ToList();

                        foreach (var route in routes)   
                        {
                            if (TryGetEndPointStations(route.Id, out string startStation, out string finishStation))
                            {
                                route.StartStation = startStation;
                                route.FinishStation = finishStation;
                            }
                        }
                    }
                }
                        #region Old Cod
                        //using (var db = new ApplicationContext(DbPath))
                        //{
                        //    if (db.Routes.Count() == 0 || SourceChanged)
                        //    {
                        //        using (var client = new HttpClient())
                        //        {
                        //            var response = client.GetAsync("http://trans-gps.cv.ua/map/api/get_routes").Result;

                        //            if (response.IsSuccessStatusCode)
                        //            {
                        //                var result = response.Content.ReadAsStringAsync().Result;
                        //                var jsonRouteList = JsonConvert.DeserializeObject<List<JsonRoute>>(result);

                        //                foreach (var jsonRoute in jsonRouteList)
                        //                {
                        //                    var route = db.Routes.Where(r => r.Id == jsonRoute.id).FirstOrDefault();
                        //                    if (route != null)
                        //                    {
                        //                        route.RouteCode = jsonRoute.code;
                        //                        route.RouteColor = jsonRoute.colour;
                        //                        route.RouteName = jsonRoute.name;
                        //                        route.Cost = jsonRoute.price;
                        //                        route.Sort = jsonRoute.sort_apk;
                        //                        route.RouteType = (RouteType)jsonRoute.bus_type_id;
                        //                    }
                        //                    else
                        //                    {
                        //                        route = new Route
                        //                        {
                        //                            Id = jsonRoute.id,
                        //                            RouteCode = jsonRoute.code,
                        //                            RouteColor = jsonRoute.colour,
                        //                            RouteName = jsonRoute.name,
                        //                            Cost = jsonRoute.price,
                        //                            Sort = jsonRoute.sort_apk,
                        //                            RouteType = (RouteType)jsonRoute.bus_type_id
                        //                        };

                        //                        db.Routes.Add(route);
                        //                    }
                        //                }
                        //            }
                        //        }

                        //        await db.Stations.AddRangeAsync(GetDummyStationList());
                        //        await db.SaveChangesAsync();

                        //        var dictRoutStation = GetDictRouteStation();

                        //        foreach (var route in db.Routes)
                        //        {
                        //            if (dictRoutStation.TryGetValue(route.Id, out List<RouteStation> listRouteStation))
                        //            {
                        //                foreach (var routeStation in listRouteStation)
                        //                {
                        //                    route.RouteStations.Add(routeStation);
                        //                }
                        //            }
                        //        }

                        //        await db.SaveChangesAsync();
                        //    }

                        //    try
                        //    {
                        //        var origRoutes = db.Routes.OrderBy(r => r.Sort).Include(r => r.RouteStations).ThenInclude(rs => rs.Station).ThenInclude(s => s.RouteStations).ToList();

                        //        routes = origRoutes.Select(r => new RouteViewModel
                        //        {
                        //            Id = r.Id,
                        //            RouteType = r.RouteType,
                        //            RouteName = r.RouteName,
                        //            Coordinates = r.Coordinates,
                        //            Cost = r.Cost,
                        //            RouteStations = r.RouteStations
                        //        }).ToList();
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        var msg = ex.Message;
                        //        throw;
                        //    }
                        //} 
                        #endregion
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    var message = x.Message;
                    System.Console.WriteLine($"Error: AggregateException at RouteRepository.GetRoutes() - {message}");

                    var typeEx = x.GetType();
                    return true;
                });
            }

            return routes;
        }

        private bool TryGetEndPointStations(int routeId, out string startStation, out string finishStation)
        {
            startStation = "";
            finishStation = "";

            if (DictEndPointStation.TryGetValue(routeId, out Tuple<string, string> endPointStations))
            {
                startStation = endPointStations.Item1;
                finishStation = endPointStations.Item2;
                return true;
            }

            return false;
        }

        private Dictionary<int, Tuple<string, string>> _dictEndPointStation;
        private Dictionary<int, Tuple<string, string>> DictEndPointStation => _dictEndPointStation ?? (_dictEndPointStation = new Dictionary<int, Tuple<string, string>>()
        {
            [-1] = new Tuple<string, string>("вул. Моріса Тореза", "з-д \"Кварц\""), //1
            [-2] = new Tuple<string, string>("ТЦ \"METRO\"", "Училище №15"), //1A
            [16] = new Tuple<string, string>("вул. П.Кільцева", "м/н Садгора"), //2
            [-3] = new Tuple<string, string>("вул. Садова", "вул. Вижницька"), //3
            [6] = new Tuple<string, string>("вул. Садова", "м/н Цецино"), //4
            [4] = new Tuple<string, string>("Університет", "вул. Ентузіастів"), //5
            [18] = new Tuple<string, string>("Університет", "вул. Воробкевича"), //5A
            [7] = new Tuple<string, string>("вул. Садова", "вул. Заставнянська"), //6
            [36] = new Tuple<string, string>("вул. Кочубея", "м/н Нові Ленківці"), //7
            [-8] = new Tuple<string, string>("вул. Садова", "м/н Клокучка"), //8
            [11] = new Tuple<string, string>("готель \"Турист\"", "завод \"Гравітон\""), //9
            [14] = new Tuple<string, string>("з-д \"Гравітон\"", "пл. Соборна"), //9A
            [-10] = new Tuple<string, string>("вул. Головна", "вул. В. Александри"), //10
            [-11] = new Tuple<string, string>("вул. Головна", "м/н Садки"), //11
            [2] = new Tuple<string, string>("Кінотеатр \"Чернівці\"", "з-д \"Кварц\""),//11
            [3] = new Tuple<string, string>("вул. П.Кільцева", "Кінотеатр \"Чернівці\""), //12
            [5] = new Tuple<string, string>("вул. Садова", "Геріатричний пансіонат"), //13
            [-14] = new Tuple<string, string>("Дріжзавод", "Калинівський ринок"), //14
            [-15] = new Tuple<string, string>("вул. Головна", "вул. Дунайська"), //15
            [-16] = new Tuple<string, string>("вул. Головна", "вул. Д. Квітовського"), //16
            [-17] = new Tuple<string, string>("вул. Садова", "вул. Д. Квітовського"), //17
            [-18] = new Tuple<string, string>("вул. Садова", "вул. Дунайська"), //18
            [-19] = new Tuple<string, string>("вул. Садова", "вул. Литовська"), //19
            [12] = new Tuple<string, string>("з-д \"Гравітон\"", "Спецкомбінат"), //20
            [-21] = new Tuple<string, string>("вул. Головна", "м/н Стара Жучка"), //21
            [-22] = new Tuple<string, string>("Університет", "Калинівський ринок"), //22
            [17] = new Tuple<string, string>("вул. Моріса Тореза", "Поліклініка"), //23
            [-24] = new Tuple<string, string>("вул. Сторожинецька", "м/н Ленківці"), //24
            [-25] = new Tuple<string, string>("вул. Шкільна", "вул. Підлісна"), //25
            [-26] = new Tuple<string, string>("вул. Комарова", "вул. Руська"), //26
            [-261] = new Tuple<string, string>("з-д \"Гравітон\"", "вул. Комарова"), //26A
            [9] = new Tuple<string, string>("вул. Садова", "вул. Я. Степового"), //27
            [13] = new Tuple<string, string>("Автовокзал", "вул. Маршала Рибалка"), //29
            [-30] = new Tuple<string, string>("Калинівський ринок", "Спецкомбінат"), //30
            [-31] = new Tuple<string, string>("Калинівський ринок", "вул. Комарова"), //31
            [-32] = new Tuple<string, string>("пр. Незалежності", "вул. Калинівська"), //32
            [-33] = new Tuple<string, string>("з-д \"Гравітон\"", "Калинівський ринок"), //33
            [-331] = new Tuple<string, string>("з-д \"Гравітон\"", "вул.Моріса Тореза"), //33A
            [10] = new Tuple<string, string>("з-д \"Гравітон\"", "м/н Боянівка"), //34
            [-35] = new Tuple<string, string>("вул. Садова", "Калинівський ринок"), //35
            [23] = new Tuple<string, string>("вул. Садова", "Чорнівський млин"), //36
            [22] = new Tuple<string, string>("вул. Садова", "Чорнівський млин"), //37
            [21] = new Tuple<string, string>("Аеропорт", "Епіцентр"), //38
            [-39] = new Tuple<string, string>("вул. Сагайдачного", "вул. Комарова"), //39
            [-41] = new Tuple<string, string>("Калинівський ринок", "вул. Перемоги"), //41
            [-42] = new Tuple<string, string>("вул. Таджицька", "Університет"), //42
            [-43] = new Tuple<string, string>("Спецкомбінат", "вул. Бережанська"), //43
            [38] = new Tuple<string, string>("Університет", "Училище №15"), //1T
            [25] = new Tuple<string, string>("Університет", "Поліклініка профоглядів"), //2T
            [27] = new Tuple<string, string>("Дріжзавод", "з-д \"Кварц\"/Училище №15"), // 3/3A
            [26] = new Tuple<string, string>("Музей Народної Архітектури", "Університет"), //4T
            [39] = new Tuple<string, string>("Калинівський ринок", "Училище №15"), //5T
            [31] = new Tuple<string, string>("пл. Соборна", "з-д \"Кварц\"/Училище №15"), //6/6A
            [33] = new Tuple<string, string>("Університет", "вул. П.Кільцева"), //8T
            [34] = new Tuple<string, string>("Садгора", "вул. П.Кільцева"), //11T
        });

        private class JsonRoute
        {
            public int id { get; set; }
            public string code { get; set; }
            public string name { get; set; }
            public int sort_apk { get; set; }
            public string colour { get; set; }
            public int bus_type_id { get; set; }
            public double price { get; set; }
        }

        #region Dummies
        private List<Station> GetDummyStationList()
        {
            var stations = new List<Station>()
            {
                new Station { Id = 1, Description = "завод \"Кварц\"" },
                new Station { Id = 2, Description = "Поліклініка з-д\"Кварц\"" },
                new Station { Id = 3, Description = "Школа №22" },
                new Station { Id = 4, Description = "Онколікарня" },
                new Station { Id = 5, Description = "Кінотеатр \"Чернівці\"" },
                new Station { Id = 6, Description = "бульвар Героїв Крут" },
                new Station { Id = 7, Description = "ТРЦ \"DEPOt\"" },
                new Station { Id = 8, Description = "Друкарня" },
                new Station { Id = 9, Description = "Центральний ринок" },
                new Station { Id = 10, Description = "вул. Сторожинецька" },
                new Station { Id = 11, Description = "Гардероб" },
                new Station { Id = 12, Description = "магазин \"Спорттовари\""},
                new Station { Id = 13, Description = "вул. Небесної сотні"},
                new Station { Id = 14, Description = "Мікрорайон"},
                new Station { Id = 15, Description = "Тролейбусне депо "},
                new Station { Id = 16, Description = "Мікрорайон"}
            };

            return stations;
        }

        private List<Route> GetDummyRoutesList()
        {
            var routes = new List<Route>();

            var route = new Route
            {
                Id = 1,
                RouteType = RouteType.Bus,
                RouteName = "11",
                Cost = 25,
                Coordinates = "48.29298 25.93292, 48.29211 25.93291, 48.29111 25.93301, 48.29041 25.93311, 48.29045 25.93365, 48.28747 25.93291, 48.28731 25.93394, 48.28376 25.93272, 48.27537 25.92963, 48.26946 25.9275, 48.26905 25.92693, 48.26891 25.92684, 48.26877 25.92685, 48.26851 25.927, 48.2682 25.92702, 48.26667 25.92652, 48.25762 25.92799, 48.25793 25.93208, 48.25823 25.93437, 48.26032 25.93944, 48.26417 25.95177, 48.25304 25.95945, 48.251 25.95273, 48.25237 25.93129, 48.25251 25.93083, 48.25307 25.93034, 48.25679 25.92815, 48.25762 25.92798"
            };
            routes.Add(route);

            route = new Route
            {
                Id = 2,
                RouteType = RouteType.Bus,
                RouteName = "12",
                Cost = 5,
                Coordinates = "48.29298 25.93292, 48.29211 25.93291, 48.29111 25.93301, 48.29041 25.93311, 48.29045 25.93365, 48.28747 25.93291, 48.28731 25.93394, 48.28376 25.93272, 48.27537 25.92963, 48.26946 25.9275, 48.26905 25.92693, 48.26891 25.92684, 48.26877 25.92685, 48.26851 25.927, 48.2682 25.92702, 48.26667 25.92652, 48.25762 25.92799, 48.25793 25.93208, 48.25823 25.93437, 48.26032 25.93944, 48.26417 25.95177, 48.25304 25.95945, 48.251 25.95273, 48.25237 25.93129, 48.25251 25.93083, 48.25307 25.93034, 48.25679 25.92815, 48.25762 25.92798"
            };
            routes.Add(route);

            route = new Route
            {
                Id = 3,
                RouteType = RouteType.Bus,
                RouteName = "9",
                Cost = 5,
                Coordinates = "48.26878 25.92826, 48.26902 25.92769, 48.26916 25.9274, 48.26911 25.92704, 48.26897 25.92687, 48.26885 25.92683, 48.26872 25.92688, 48.2686 25.92703, 48.26855 25.92719, 48.26859 25.92756, 48.26867 25.92817, 48.26709 25.93956, 48.26974 25.94796, 48.27052 25.95027, 48.26976 25.95118, 48.27084 25.95338, 48.27136 25.95501, 48.26781 25.95938, 48.26962 25.96258, 48.26985 25.96264, 48.27135 25.96345, 48.27246 25.96348, 48.27493 25.96029"
            };
            routes.Add(route);

            route = new Route
            {
                Id = 4,
                RouteType = RouteType.Trolleybus,
                RouteName = "26-A",
                Cost = 5,
                Coordinates = "48.25104 25.9519, 48.25124 25.95163, 48.25128 25.9509, 48.25112 25.95062, 48.25104 25.9519, 48.251 25.9527, 48.25305 25.95946, 48.28918 25.93438, 48.29243 25.93557, 48.2964 25.93791, 48.2968 25.93769, 48.29983 25.93292, 48.30231 25.92551, 48.30507 25.92366, 48.30548 25.92203, 48.30731 25.91954, 48.30967 25.91824, 48.31031 25.91817, 48.31092 25.91778, 48.31325 25.91869"
            };
            routes.Add(route);

            route = new Route
            {
                Id = 5,
                RouteType = RouteType.Trolleybus,
                RouteName = "33-A",
                Cost = 5,
                Coordinates = "48.29298 25.93292, 48.29211 25.93291, 48.29111 25.93301, 48.29041 25.93311, 48.29045 25.93365, 48.28747 25.93291, 48.28731 25.93394, 48.28376 25.93272, 48.27537 25.92963, 48.26946 25.9275, 48.26905 25.92693, 48.26891 25.92684, 48.26877 25.92685, 48.26851 25.927, 48.2682 25.92702, 48.26667 25.92652, 48.25762 25.92799, 48.25793 25.93208, 48.25823 25.93437, 48.26032 25.93944, 48.26417 25.95177, 48.25304 25.95945, 48.251 25.95273, 48.25124 25.95164, 48.25128 25.9509, 48.25111 25.95062, 48.25104 25.9519"
            };
            routes.Add(route);

            return routes;
        }

        private Dictionary<int, List<RouteStation>> GetDictRouteStation()
        {
            var dictRouteStation = new Dictionary<int, List<RouteStation>>();
            dictRouteStation[1] = new List<RouteStation>
                {
                    new RouteStation { RouteId = 2, StationId = 1, Direction = 1, NumberOnRoute = 0, DistanceToNext = 396},
                    new RouteStation { RouteId = 2, StationId = 2, Direction = 1, NumberOnRoute = 1, DistanceToNext = 313 },
                    new RouteStation { RouteId = 2, StationId = 3, Direction = 1, NumberOnRoute = 2,  DistanceToNext = 936},
                    new RouteStation { RouteId = 2, StationId = 4, Direction = 1, NumberOnRoute = 7, DistanceToNext =  677},
                    new RouteStation { RouteId = 2, StationId = 5, Direction = -1, NumberOnRoute = 0, DistanceToNext =  63},
                    new RouteStation { RouteId = 2, StationId = 9, Direction = -1, NumberOnRoute = 3, DistanceToNext =  454},
                    new RouteStation { RouteId = 2, StationId = 10, Direction = -1, NumberOnRoute = 4, DistanceToNext =  295},
                    new RouteStation { RouteId = 2, StationId = 11, Direction = -1, NumberOnRoute = 8, DistanceToNext =  463}
                };

            dictRouteStation[2] = new List<RouteStation>
                {
                    new RouteStation { RouteId = 2, StationId = 1, Direction = 1, NumberOnRoute = 0, DistanceToNext = 396},
                    new RouteStation { RouteId = 2, StationId = 2, Direction = 1, NumberOnRoute = 1, DistanceToNext = 313 },
                    new RouteStation { RouteId = 2, StationId = 3, Direction = 1, NumberOnRoute = 2,  DistanceToNext = 936},
                    new RouteStation { RouteId = 2, StationId = 4, Direction = 1, NumberOnRoute = 7, DistanceToNext =  677},
                    new RouteStation { RouteId = 2, StationId = 5, Direction = -1, NumberOnRoute = 0, DistanceToNext =  63},
                    new RouteStation { RouteId = 2, StationId = 9, Direction = -1, NumberOnRoute = 3, DistanceToNext =  454},
                    new RouteStation { RouteId = 2, StationId = 10, Direction = -1, NumberOnRoute = 4, DistanceToNext =  295},
                    new RouteStation { RouteId = 2, StationId = 11, Direction = -1, NumberOnRoute = 8, DistanceToNext =  463}
                };

            dictRouteStation[3] = new List<RouteStation>
                {
                    new RouteStation { RouteId = 3, StationId = 15, Direction = 1, NumberOnRoute = 0, DistanceToNext = 515},
                    new RouteStation { RouteId = 3, StationId = 16, Direction = 1, NumberOnRoute = 1, DistanceToNext = 304 },
                    new RouteStation { RouteId = 3, StationId = 12, Direction = -1, NumberOnRoute = 11,  DistanceToNext = 339},
                    new RouteStation { RouteId = 3, StationId = 13, Direction = -1, NumberOnRoute = 12, DistanceToNext =  380},
                    new RouteStation { RouteId = 3, StationId = 14, Direction = -1, NumberOnRoute = 13, DistanceToNext =  952},
                };

            dictRouteStation[4] = new List<RouteStation>
                {
                    new RouteStation { RouteId = 4, StationId = 6, Direction = -1, NumberOnRoute = 3, DistanceToNext = 357},
                    new RouteStation { RouteId = 4, StationId = 7, Direction = -1, NumberOnRoute = 4, DistanceToNext = 413 },
                    new RouteStation { RouteId = 4, StationId = 8, Direction = -1, NumberOnRoute = 5,  DistanceToNext = 346},
                };

            dictRouteStation[5] = new List<RouteStation>
                {
                    new RouteStation { RouteId = 5, StationId = 5, Direction = -1, NumberOnRoute = 0, DistanceToNext =  63},
                    new RouteStation { RouteId = 5, StationId = 9, Direction = -1, NumberOnRoute = 3, DistanceToNext =  454},
                    new RouteStation { RouteId = 5, StationId = 10, Direction = -1, NumberOnRoute = 4, DistanceToNext =  295},
                    new RouteStation { RouteId = 5, StationId = 4, Direction = 1, NumberOnRoute = 8, DistanceToNext =  677}
                };

            return dictRouteStation;
        } 
        #endregion
    }
}
