﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using TransGps.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Newtonsoft.Json;

namespace TransGps.Repositories
{
    class UnitTypeRepository : RepositoryBase
    {
        public async Task<Dictionary<int, string>> GetUnitTypes()
        {
            var unitTypes = new Dictionary<int, string>();

            try
            {
                using (var db = new ApplicationContext(DbPath))
                {
                    if (db.UnitTypes.Count() == 0 || SourceChanged)
                    {
                        using (var client = new HttpClient())
                        {
                            var response = client.GetAsync("https://trans-gps.cv.ua/map/api/get_bus_type").Result;

                            if (response.IsSuccessStatusCode)
                            {
                                var result = response.Content.ReadAsStringAsync().Result;
                                var jsonUnitTypeList = JsonConvert.DeserializeObject<List<JsonUnitType>>(result);

                                foreach (var jsonUnitType in jsonUnitTypeList)
                                {
                                    var unitType = db.UnitTypes.Where(u => u.Id == jsonUnitType.id).FirstOrDefault();

                                    if (unitType != null)
                                    {
                                        if (String.Compare(unitType.Name, jsonUnitType.name) != 0)
                                        {
                                            unitType.Name = jsonUnitType.name;
                                        }
                                    }
                                    else
                                    {
                                        unitType = new UnitType { Id = jsonUnitType.id, Name = jsonUnitType.name };
                                        db.UnitTypes.Add(unitType);
                                    }

                                }
                            }
                            
                            await db.SaveChangesAsync();
                        }
                    }

                    unitTypes = db.UnitTypes.ToDictionary(u => u.Id, u => u.Name);
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    var message = x.Message;
                    var typeEx = x.GetType();
                    return true;
                });
            }

            return unitTypes;
        }

        private class JsonUnitType
        {
            public int id { get; set; }
            public string name { get; set; }
        }
    }
}
