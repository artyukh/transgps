﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TransGps.Interfaces
{
    public interface IVersionComparer
    {
        Task<bool> IsUsingLatestVersion();
        Task OpenAppInStore();
    }
}
