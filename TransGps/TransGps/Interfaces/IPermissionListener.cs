﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TransGps.Interfaces
{
    public interface IPermissionListener
    {
        Task AwaitPermissionsRequest();
    }
}
