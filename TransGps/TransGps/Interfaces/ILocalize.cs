﻿using System.Globalization;

namespace TransGps.Interfaces
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCiltureInfo();
    }
}
