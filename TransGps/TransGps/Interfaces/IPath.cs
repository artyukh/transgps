﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransGps.Interfaces
{
    public interface IPath
    {
        string GetDataBasePath(string fileName);
    }
}
