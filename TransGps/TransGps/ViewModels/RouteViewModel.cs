﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using TransGps.Helpers;
using TransGps.Models;

namespace TransGps.ViewModels
{
    public class RouteViewModel : BaseViewModel
    {
        private Route route;
        private Route Route => route ?? (route = new Route());

        public RouteType RouteType
        {
            get => Route.RouteType;
            set
            {
                if (value != Route.RouteType)
                {
                    Route.RouteType = value;
                    OnPropertyChanged(nameof(RouteType));
                }
            }
        }

        public int Id
        {
            get => Route.Id;
            set
            {
                if (value != Route.Id)
                {
                    Route.Id = value;
                    OnPropertyChanged(nameof(Id));
                }
            }
        }

        public string RouteName
        {
            get => Route.RouteName;
            set
            {
                if (value != Route.RouteName)
                {
                    Route.RouteName = value;
                    OnPropertyChanged(nameof(RouteName));
                }
            }
        }

        private string _startStation;
        public string StartStation
        {
            get => _startStation;
            set
            {
                if (value != _startStation)
                {
                    _startStation = value;
                    OnPropertyChanged(nameof(StartStation));
                }
            }
        }

        private string _finishStation;
        public string FinishStation
        {
            get => _finishStation;
            set
            {
                if (value != _finishStation)
                {
                    _finishStation = value;
                    OnPropertyChanged(nameof(FinishStation));
                }
            }
        }

        public string Coordinates
        {
            get => Route.Coordinates;
            set
            {
                if (value != Route.Coordinates)
                {
                    Route.Coordinates = value;
                    OnPropertyChanged(nameof(Coordinates));
                }
            }
        }

        public List<RouteStation> RouteStations
        {
            get => Route.RouteStations;
            set
            {
                if (value != Route.RouteStations)
                {
                    Route.RouteStations = value;
                    OnPropertyChanged(nameof(RouteStations));
                }
            }
        }

        public string EndPointsStations => $"{StartStation} - {FinishStation}";

        public string RouteImagePath => RouteType == RouteType.Bus ? "Bus.png" : "Troll.png";

        private bool isFavorite;
        public bool IsFavorite
        {
            get => isFavorite;
            set
            {
                if (value != isFavorite)
                {
                    isFavorite = value;

                    var favoriteRoutes = Settings.FavoriteRoutesListSettings;

                    if (value)
                    {
                        if (!favoriteRoutes.Contains(Id))
                            favoriteRoutes.Add(Id);
                    }
                    else
                    {
                        favoriteRoutes.Remove(Id);
                    }

                    Settings.FavoriteRoutesListSettings = favoriteRoutes;

                    OnPropertyChanged(nameof(IsFavorite));
                }
            }
        }

        private bool isRouteChecked;
        public bool IsRouteChecked
        {
            get => isRouteChecked;
            set
            {
                if (value != isRouteChecked)
                {
                    isRouteChecked = value;

                    var checkedRoutes = Settings.CheckedRoutesListSettings;

                    if (value)
                    {
                        if (!checkedRoutes.Contains(Id))
                            checkedRoutes.Add(Id);
                    }
                    else
                    {
                        checkedRoutes.Remove(Id);
                    }

                    Settings.CheckedRoutesListSettings = checkedRoutes;

                    OnPropertyChanged(nameof(IsRouteChecked));
                }
            }
        }

        public double Cost
        {
            get => Route.Cost;
            set
            {
                if (value != Route.Cost)
                {
                    Route.Cost = value;
                }
            }
        }

        public string RouteColor
        {
            get => Route.RouteColor;
            set
            {
                if (value != Route.RouteColor)
                {
                    Route.RouteColor = value;
                }
            }
        }

        public string Distance
        {
            get => Route.Distance;
            set
            {
                if (value != Route.Distance)
                {
                    Route.Distance = value;
                    OnPropertyChanged(nameof(Distance));
                }
            }
        }

        public string Interval
        {
            get => Route.Interval;
            set
            {
                if (value != Route.Interval)
                {
                    Route.Interval = value;
                    OnPropertyChanged(nameof(Interval));
                }
            }
        }

        public string WorkHours
        {
            get => Route.WorkHours;
            set
            {
                if (value != Route.WorkHours)
                {
                    Route.WorkHours = value;
                    OnPropertyChanged(nameof(WorkHours));
                }
            }
        }

        public List<Station> StationList => RouteStations.Where(s => s.Direction > 0 && s.NumberOnRoute != 0).OrderBy(rs => rs.NumberOnRoute).Select(rs => rs.Station).ToList();

        public List<Station> StationListDesc => RouteStations.Where(s => s.Direction < 0 && s.NumberOnRoute != 0).OrderBy(rs => rs.NumberOnRoute).Select(rs => rs.Station).ToList();
    }
}
