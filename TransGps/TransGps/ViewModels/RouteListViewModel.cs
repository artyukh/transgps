﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TransGps.Pages;
using TransGps.Repositories;
using Xamarin.Forms;
using System.Windows.Input;
using TransGps.Models;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;
using Plugin.Geolocator;
using TransGps.Interfaces;
using TransGps.Helpers;
using System.Threading;
using Xamarin.Essentials;

namespace TransGps.ViewModels
{
    public class RouteListViewModel : BaseViewModel
    {
        private ImageRepository _imageRepository;

        public RouteListViewModel()
        {
            _imageRepository = new ImageRepository();
            GetPermissionRequest();
            LoadBanners();
        }

        private List<RouteViewModel> defaultRoutes;

        private List<RouteViewModel> DefaultRoutes
        {
            get
            {
                if (defaultRoutes == null && !RouteLoding)
                {
                    Task.Run(() =>
                    {
                        RouteLoding = true;

                        var routeRepository = new RouteRepository();
                        defaultRoutes = routeRepository.Routes.Where(r => r.Id != 20 && r.Id != 37).ToList();

                        #region Debug

                        //try
                        //{
                        //defaultRoutes = routeRepository.Routes.Where(r => r.Id != 20 && r.Id != 37).ToList();
                        //}
                        //catch (Exception e)
                        //{
                        //    System.Console.WriteLine($"Error: Exception at RouteListViewModel.DefaultRoutes - {e.Message}");
                        //    if (e.InnerException != null)
                        //    {
                        //        System.Console.WriteLine($"Error: InnerException - {e.Message}");
                        //    }
                        //} 

                        #endregion

                        var favoriteRoutes = Settings.FavoriteRoutesListSettings;
                        var checkedRoutes = Settings.CheckedRoutesListSettings;

                        foreach (var route in defaultRoutes)
                        {
                            if (favoriteRoutes.Contains(route.Id))
                                route.IsFavorite = true;

                            if (checkedRoutes.Contains(route.Id))
                                route.IsRouteChecked = true;
                        }

                        RouteLoding = false;
                        MessagingCenter.Send(this, "NeedUpdateLayers");
                    });
                }

                return defaultRoutes;
            }
        }

        private ObservableCollection<RouteViewModel> routes;

        public ObservableCollection<RouteViewModel> Routes
        {
            get
            {
                if (routes == null)
                {
                    routes = new ObservableCollection<RouteViewModel>();

                    for (int i = 0; i < DefaultRoutes.Count; i++)
                    {
                        routes.Add(DefaultRoutes[i]);
                    }
                }

                return routes;
            }
        }

        private ObservableCollection<Banner> _banners;
        public ObservableCollection<Banner> Banners => _banners ?? (_banners = new ObservableCollection<Banner>());

        private int _bannerListPosition;

        public int BannerListPosition
        {
            get => _bannerListPosition;
            set
            {
                if (value != _bannerListPosition)
                {
                    _bannerListPosition = value;
                    OnPropertyChanged(nameof(BannerListPosition));
                }
            }
        }

        #region Commands

        private ICommand _showRouteDetail;

        public ICommand ShowRouteDetail => _showRouteDetail ?? (_showRouteDetail = new Command(async (e) =>
        {
            var route = e as RouteViewModel;
            await Navigation.PushAsync(new RouteDetailPage(route));
        }));

        private ICommand _showBusRoutes;

        public ICommand ShowBusRoutes => _showBusRoutes ?? (_showBusRoutes = new Command(async () =>
        {
            IsBusRoutesToggled = true;
            await Navigation.PushAsync(new CheckRoutePage(this));
        }));

        private ICommand _showTrolleyBusRoutes;

        public ICommand ShowTrolleyBusRoutes => _showTrolleyBusRoutes ?? (_showTrolleyBusRoutes = new Command(async () =>
        {
            IsTrolleyRoutesToggled = true;
            await Navigation.PushAsync(new CheckRoutePage(this));
        }));

        private ICommand _clearAll;

        public ICommand ClearAll => _clearAll ?? (_clearAll = new Command(() =>
        {
            foreach (var route in DefaultRoutes)
            {
                route.IsRouteChecked = false;
            }

            Position = null;

            MessagingCenter.Send(this, "NeedUpdateLayers");
        }));

        private ICommand _clearCurrentList;

        public ICommand ClearCurrentList => _clearCurrentList ?? (_clearCurrentList = new Command(() =>
        {
            foreach (var route in Routes)
            {
                route.IsRouteChecked = false;
            }
        }));

        private ICommand _showCurrentLocation;

        public ICommand ShowCurrentLocation => _showCurrentLocation ?? (_showCurrentLocation = new Command(async () =>
        {
            try
            {
                if (!await DependencyService.Get<IGpsEnable>().EnableGpsAsync())
                    return;

                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 10;

                Position = await locator.GetLastKnownLocationAsync();

                if (Position == null)
                {
                    Position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null);
                }

                if (Position != null)
                {
                    OnPropertyChanged(nameof(IsAnySelected));
                    MessagingCenter.Send<RouteListViewModel>(this, "location");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unable to get location - {ex.Message}");

                if (ex.InnerException != null)
                    Console.WriteLine($"Inner Exception - {ex.InnerException}");
            }
        }));

        public ICommand GoToLink => new Command<Banner>(GoToLinkExecute);

        private async void GoToLinkExecute(Banner currentBanner)
        {
            if (currentBanner == null)
                return;

            await Launcher.TryOpenAsync(currentBanner.Link);
            await _imageRepository.ClickOnBanner(currentBanner.Id);
        }

        #endregion

        public INavigation Navigation { get; set; }

        public Position Position { get; set; }

        public bool IsLocationSupported => CrossGeolocator.IsSupported;

        private bool _taskRunning;

        public bool RouteLoding
        {
            get => _taskRunning;
            set
            {
                if (value != _taskRunning)
                {
                    _taskRunning = value;
                    ButtonsEnabled = !value;
                    OnPropertyChanged(nameof(RouteLoding));
                }
            }
        }

        private bool _buttonsEnabled;

        public bool ButtonsEnabled
        {
            get => _buttonsEnabled;
            set
            {
                if (value != _buttonsEnabled)
                {
                    _buttonsEnabled = value;
                    OnPropertyChanged(nameof(ButtonsEnabled));
                    OnPropertyChanged(nameof(LocationButtonEnabled));
                }
            }
        }

        public bool LocationButtonEnabled
        {
            get
            {
                var locator = CrossGeolocator.Current;

                return locator.IsGeolocationAvailable && ButtonsEnabled; // && locator.IsGeolocationEnabled
            }
        }

        private RouteViewModel selectedRoute;

        public RouteViewModel SelectedRoute
        {
            get => selectedRoute;
            set
            {
                if (value != selectedRoute)
                {
                    selectedRoute = value;
                    OnPropertyChanged(nameof(SelectedRoute));
                }
            }
        }

        private bool isBusRoutesToggled;

        public bool IsBusRoutesToggled
        {
            get => isBusRoutesToggled;
            set
            {
                if (value != isBusRoutesToggled)
                {
                    isBusRoutesToggled = value;

                    if (isBusRoutesToggled)
                    {
                        IsTrolleyRoutesToggled = false;
                        IsFavoriteRoutesToggled = false;
                        UpdateRouteList();
                    }

                    OnPropertyChanged(nameof(IsBusRoutesToggled));
                }
            }
        }

        private bool isTrolleyRoutesToggled;

        public bool IsTrolleyRoutesToggled
        {
            get => isTrolleyRoutesToggled;
            set
            {
                if (value != isTrolleyRoutesToggled)
                {
                    isTrolleyRoutesToggled = value;

                    if (isTrolleyRoutesToggled)
                    {
                        IsBusRoutesToggled = false;
                        IsFavoriteRoutesToggled = false;
                        UpdateRouteList();
                    }

                    OnPropertyChanged(nameof(IsTrolleyRoutesToggled));
                }
            }
        }

        private bool isFavoriteRoutesToggled;

        public bool IsFavoriteRoutesToggled
        {
            get => isFavoriteRoutesToggled;
            set
            {
                if (value != isFavoriteRoutesToggled)
                {
                    isFavoriteRoutesToggled = value;

                    if (isFavoriteRoutesToggled)
                    {
                        IsBusRoutesToggled = false;
                        IsTrolleyRoutesToggled = false;
                        UpdateRouteList();
                    }

                    OnPropertyChanged(nameof(IsFavoriteRoutesToggled));
                }
            }
        }

        public bool IsAnySelected => (DefaultRoutes?.Any(r => r.IsRouteChecked) ?? false) || Position != null;

        public List<RouteViewModel> SelectedRoutes
        {
            get
            {
                OnPropertyChanged(nameof(IsAnySelected));
                return DefaultRoutes?.Where(r => r.IsRouteChecked).ToList() ?? new List<RouteViewModel>();
            }
        }


        public void UpdateLocationButtonStatus()
        {
            OnPropertyChanged(nameof(LocationButtonEnabled));
        }

        private void UpdateRouteList()
        {
            var updatedRoutes = DefaultRoutes.Where(r => (IsBusRoutesToggled && r.RouteType == RouteType.Bus) ||
                                                         (IsTrolleyRoutesToggled && r.RouteType == RouteType.Trolleybus) ||
                                                         (IsFavoriteRoutesToggled && r.IsFavorite) ||
                                                         (!IsBusRoutesToggled && !IsTrolleyRoutesToggled && !IsFavoriteRoutesToggled)).ToList();

            Routes.Clear();

            for (int i = 0; i < updatedRoutes.Count; i++)
            {
                Routes.Add(updatedRoutes[i]);
            }
        }

        private async void GetPermissionRequest()
        {
            var locator = CrossGeolocator.Current;

            if (locator.IsGeolocationAvailable)
                return;
            await DependencyService.Get<IPermissionListener>().AwaitPermissionsRequest();
            OnPropertyChanged(nameof(LocationButtonEnabled));
        }

        private async void LoadBanners()
        {
            await Task.Run(() =>
            {
                var source = _imageRepository.BannerList;

                Device.BeginInvokeOnMainThread(() =>
                {
                    foreach (var banner in source)
                    {
                        Banners.Add(banner);
                    }

                    var lastStartPosition = Settings.BannerPositionSettings;
                    if (lastStartPosition < Banners.Count - 1)
                    {
                        lastStartPosition++;
                        BannerListPosition = lastStartPosition;
                    }
                    else
                    {
                        lastStartPosition = 0;
                    }

                    Settings.BannerPositionSettings = lastStartPosition;
                });
            });

            StartSlideShow();
        }

        private async void StartSlideShow()
        {
            if (Banners.Count == 0)
                return;

            await Task.Run(async () =>
            {
                while (true)
                {
                    Thread.Sleep(3000);

                    if (BannerListPosition < Banners.Count - 1)
                    {
                        BannerListPosition++;
                    }
                    else
                    {
                        BannerListPosition = 0;
                    }

                    await _imageRepository.ShowBanner(Banners[BannerListPosition].Id);
                }
            });
        }
    }
}