﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransGps.Models
{
    class UnitType
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
