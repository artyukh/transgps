using Newtonsoft.Json;

namespace TransGps.Models
{
    public class BannerRequestModel
    {
        [JsonProperty("ads_id")]
        public string Id { get; set; }
    }
}