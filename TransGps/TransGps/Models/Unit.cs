﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransGps.Models
{
    class Unit
    {
        public int Id { get; set; }

        public int UnitTypeId { get; set; }

        public string UnitType { get; set; }

        public string UnitName { get; set; }

        public int RouteId { get; set; }

        public double Longtitude { get; set; }

        public double Lattitude { get; set; }

        public double Rotation { get; set; }

        public double Speed { get;set; }
    }
}
