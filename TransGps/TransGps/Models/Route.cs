﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransGps.Models
{
    public class Route
    {
        public RouteType RouteType { get; set; }

        public int Id { get; set; }

        public int Sort { get; set; }

        public string RouteCode { get; set; }

        public string RouteName { get; set; }

        public string Coordinates { get; set; }

        public double Cost { get; set; }

        public string RouteColor { get; set; }

        public string Distance { get; set; }

        public string Interval { get; set; }

        public string WorkHours { get; set; }

        private List<RouteStation> _routeStations;
        public List<RouteStation> RouteStations
        {
            get => _routeStations ?? (_routeStations = new List<RouteStation>());
            set => _routeStations = value;
        }
    }
}
