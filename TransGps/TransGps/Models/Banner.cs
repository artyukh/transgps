﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace TransGps.Models
{
    public class Banner
    {
        public int Id { get; set; }

        [JsonProperty("categories")]
        public string Categories { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("sortord")]
        public int SortOrder { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("img_base64")]
        public string ImgBase64
        {
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    var stream = new MemoryStream(Convert.FromBase64String(Regex.Replace(value, @"data:image/[a-z]{3};base64,", "")));
                    Image = Xamarin.Forms.ImageSource.FromStream(() => stream);
                }
            }
        }

        [JsonIgnore]
        public Xamarin.Forms.ImageSource Image { get; set; }
    }
}
