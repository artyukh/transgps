﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransGps.Models
{
    public class RouteStation
    {
        public int RouteId { get; set; }
        public Route Route { get; set; }

        public int StationId { get; set; }
        public Station Station { get; set; }

        public int Direction { get; set; }

        public int NumberOnRoute { get; set; }

        public int DistanceToNext { get; set; }
    }
}
