﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransGps.Models
{
    public class Station
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public double Longtitude { get; set; }

        public double Lattitude { get; set; }

        private List<RouteStation> _routeStations;
        public List<RouteStation> RouteStations
        {
            get => _routeStations ?? (_routeStations =  new List<RouteStation>());
            set => _routeStations = value;
        }
    }
}
