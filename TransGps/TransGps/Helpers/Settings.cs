﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TransGps.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }


        private static string FavoriteRoutesSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(IsFavoriteKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IsFavoriteKey, value);
            }
        }

        private static string CheckedRoutesSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(IsRouteCheckedKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IsRouteCheckedKey, value);
            }
        }


        private static char[] splitSeparator = new char[] { ';' };

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private const string IsFavoriteKey = "IsFavorite";
        private const string IsRouteCheckedKey = "IsRouteChecked";
        private const string BannerPositionKey = "BannerPosition";
        private static readonly string SettingsDefault = string.Empty;

        #endregion

        private static string GetStringFromList(List<int> sourceList)
        {
            return String.Join(";", sourceList);
        }

        private static List<int> GetListFromString(string source)
        {
            var sourceArray = source.Split(splitSeparator, System.StringSplitOptions.RemoveEmptyEntries);
            return sourceArray.Select(r => Convert.ToInt32(r)).ToList();
        }

        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }

        public static List<int> FavoriteRoutesListSettings
        {
            get => GetListFromString(FavoriteRoutesSettings);
            set => FavoriteRoutesSettings = GetStringFromList(value);
        }

        public static List<int> CheckedRoutesListSettings
        {
            get => GetListFromString(CheckedRoutesSettings);
            set => CheckedRoutesSettings = GetStringFromList(value);
        }
        public static int BannerPositionSettings
        {
            get => AppSettings.GetValueOrDefault(BannerPositionKey, -1);
            set => AppSettings.AddOrUpdateValue(BannerPositionKey, value);
        }
    }
}
