using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TransGps.Helpers
{
    public class LoggingMessageHandler : DelegatingHandler
    {
        public LoggingMessageHandler(HttpMessageHandler innerHandler)
            : base(innerHandler ?? new HttpClientHandler())
        {
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Debug.WriteLine("Request:");
            Debug.WriteLine(request.ToString());
            if (request.Content != null)
            {
                Debug.WriteLine(await request.Content.ReadAsStringAsync());
            }

            var response = await base.SendAsync(request, cancellationToken);

            Debug.WriteLine("Response:");
            Debug.WriteLine(response.ToString());
            if (response.Content == null)
                return response;

            var bytes = await response.Content.ReadAsByteArrayAsync();
            Debug.WriteLine(bytes.Length);
            Debug.WriteLine(await response.Content.ReadAsStringAsync());

            return response;
        }
    }
}