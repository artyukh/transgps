﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using TransGps.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TransGps.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CheckRoutePage : ContentPage
    {
        public CheckRoutePage(RouteListViewModel routeListViewModel)
        {
            InitializeComponent();
            BindingContext = routeListViewModel;
        }

        private void MyListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var routeViewModel = (RouteViewModel)e.Item;

            if (routeViewModel == null)
            {
                Console.WriteLine("Error: LitsView Item tapped is not recognized");
                return;
            }

            routeViewModel.IsRouteChecked ^= true;
        }
    }
}
