﻿using BruTile.Predefined;
using Mapsui.Layers;
using Mapsui.Projection;
using Xamarin.Forms;
using TransGps.MapLayers;
using TransGps.ViewModels;
using Xamarin.Forms.Xaml;

namespace TransGps.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        RouteLayer routeLayer;
        UnitLayer unitLayer;
        RouteListViewModel routeListViewModel;
        LocationLayer locationLayer;

        public MapPage()
        {
            InitializeComponent();
            InitializeMap();

            routeListViewModel = new RouteListViewModel { Navigation = this.Navigation };
            BindingContext = routeListViewModel;

            MessagingCenter.Subscribe<RouteListViewModel>(this, "NeedUpdateLayers", (sender) => { Updatelayers(); });
            MessagingCenter.Subscribe<RouteListViewModel>(this, "location", (sender) => { NavigateToLocation(sender); });
            MessagingCenter.Subscribe<RouteListViewModel>(this, "LocationIsNotAvailable", async (sender) =>
            {
                await DisplayAlert("", "", TransGps.Resources.Resource.Close);
            });
        }

        async void ShowPopup(string name, string speed)
        {
            var msg = $"{TransGps.Resources.Resource.Speed}: {speed} {TransGps.Resources.Resource.SpeedMeasure}";
            await DisplayAlert(name, msg, TransGps.Resources.Resource.Close);
        }


        private void NativeMap_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            System.Console.WriteLine($"DEBUG-PropertyName is - {e.PropertyName}");
        }

        private async void CheckRoutes_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new CheckRoutePage(routeListViewModel));
        }

        private void Btn_menu_Clicked(object sender, System.EventArgs e)
        {
            MessagingCenter.Send<Page>(this, "OpenMenu");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Updatelayers();
        }

         
        private void InitializeMap()
        {
            mapControl.NativeMap.CRS = "EPSG:3857";

            var mapLayer = CreateTileLayer();
            mapControl.NativeMap.Layers.Add(mapLayer);

            routeLayer = RouteLayer.CreateRouteLayer();
            mapControl.NativeMap.Layers.Add(routeLayer);

            unitLayer = UnitLayer.CreateUnitLayer();
            mapControl.NativeMap.Layers.Add(unitLayer);
            mapControl.NativeMap.InfoLayers.Add(unitLayer);

            locationLayer = LocationLayer.CreateLayer();
            mapControl.NativeMap.Layers.Add(locationLayer);

            SetupCenterMap();

            mapControl.NativeMap.Info += (s, e) =>
            {
                var featureLabelName = e.MapInfo.Feature?["Name"]?.ToString();
                var featureLabelSpeed = e.MapInfo.Feature?["Speed"]?.ToString();
                if (!string.IsNullOrWhiteSpace(featureLabelName))
                {
                    ShowPopup(featureLabelName, featureLabelSpeed);
                }
            };
        }

        

        private TileLayer CreateTileLayer()
        {
            var tileSource = new BruTile.Web.HttpTileSource(new GlobalSphericalMercator(12, 19), "https://osm.trans-gps.cv.ua/osm_tiles/{z}/{x}/{y}.png");
            return new TileLayer(tileSource);
        }

        private void Updatelayers()
        {
            var viewModel = BindingContext as RouteListViewModel;
            if (viewModel == null || viewModel.RouteLoding)
            {
                return;
            }

            SetupCenterMap();

            routeLayer.SelectedRoutes = viewModel.SelectedRoutes;
            routeLayer.UpdateData();

            unitLayer.SelectedRoutes = viewModel.SelectedRoutes;
            unitLayer.UpdateData();

            locationLayer.Location = null;

            viewModel.UpdateLocationButtonStatus();
        }

        private void SetupCenterMap()
        {
            var center = new Mapsui.Geometries.Point(25.93286, 48.28820);
            var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(center.X, center.Y);
            mapControl.NativeMap.NavigateTo(sphericalMercatorCoordinate);
            mapControl.NativeMap.NavigateTo(mapControl.NativeMap.Resolutions[0]);
            mapControl.NativeMap.PropertyChanged += NativeMap_PropertyChanged;
        }

        private void NavigateToLocation(RouteListViewModel routeListViewModel)
        {
            var location = new Mapsui.Geometries.Point(routeListViewModel.Position.Longitude, routeListViewModel.Position.Latitude);
            var coordinate = SphericalMercator.FromLonLat(location.X, location.Y);
            mapControl.NativeMap.NavigateTo(coordinate);
            mapControl.NativeMap.NavigateTo(mapControl.NativeMap.Resolutions[4]);

            locationLayer.Location = coordinate;
        }
    }
}
