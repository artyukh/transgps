﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransGps.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TransGps.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RouteCommonDetailPage : ContentPage
	{
		public RouteCommonDetailPage (RouteViewModel route)
		{
			InitializeComponent ();
            BindingContext = route;
		}
	}
}