﻿using System;
using System.ComponentModel;
using System.Globalization;
using Xamarin.Forms;

namespace TransGps.Converters
{
    class EnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var descriptioAttrCollection = value?.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            return descriptioAttrCollection != null && descriptioAttrCollection.Length > 0 ? ((DescriptionAttribute)descriptioAttrCollection.GetValue(0)).Description : value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
