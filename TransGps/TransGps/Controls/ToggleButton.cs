﻿using System;
using Xamarin.Forms;

namespace TransGps.Controls
{
    class ToggleButton : ImageButton
    {
        public event EventHandler<ToggledEventArgs> Toggled;

        public event EventHandler OnlyToggledByClickingChanged;

        public static BindableProperty IsToggledProperty = BindableProperty.Create("IsToggled", 
                                                                                    typeof(bool), 
                                                                                    typeof(ToggleButton), 
                                                                                    false,
                                                                                    BindingMode.TwoWay,
                                                                                    propertyChanged: OnIsToggleChanged);

        public static BindableProperty OnlyToggledByClickingProperty = BindableProperty.Create("OnlyToggledByClicking",
                                                                                                typeof(bool),
                                                                                                typeof(ToggleButton),
                                                                                                false,
                                                                                                BindingMode.TwoWay,
                                                                                                propertyChanged: OnOnlyToggledByClickingChanged);

        public ToggleButton()
        {
            Clicked += (sender, args) => IsToggled = OnlyToggledByClicking ? true : IsToggled ^= true;
        }

        public bool IsToggled
        {
            set => SetValue(IsToggledProperty, value);
            get => (bool)GetValue(IsToggledProperty);
        }

        public bool OnlyToggledByClicking
        {
            set => SetValue(OnlyToggledByClickingProperty, value);
            get => (bool)GetValue(OnlyToggledByClickingProperty);
        }

        protected override void OnParentSet()
        {
            base.OnParentSet();
            VisualStateManager.GoToState(this, "ToggleOff");
        }

        private static void OnIsToggleChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var toggleButton = (ToggleButton)bindable;
            var isToggled = (bool)newValue;

            toggleButton.Toggled?.Invoke(toggleButton, new ToggledEventArgs(isToggled));

            VisualStateManager.GoToState(toggleButton, isToggled ? "ToggleOn" : "ToggleOff");
        }

        private static void OnOnlyToggledByClickingChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var toggleButton = (ToggleButton)bindable;
            toggleButton.OnlyToggledByClickingChanged?.Invoke(toggleButton, null);
        }
    }
}
