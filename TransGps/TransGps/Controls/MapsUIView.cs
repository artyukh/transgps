﻿using Mapsui.Styles;

namespace TransGps.Controls
{
    public class MapsUIView : Xamarin.Forms.View
    {
        public Mapsui.Map NativeMap { get; }

        public MapsUIView()
        {
            NativeMap = new Mapsui.Map();
            NativeMap.BackColor = Color.Black; //This Color should match the map - I prefer Black over White here
        }
    }
}
