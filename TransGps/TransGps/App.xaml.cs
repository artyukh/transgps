﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using TransGps.Pages;
using TransGps.Resources;
using TransGps.Interfaces;
using TransGps.Repositories;
using TransGps.Services;
using Plugin.Connectivity;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace TransGps
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.Android || Device.RuntimePlatform == Device.iOS)
            {
                Resource.Culture = DependencyService.Get<ILocalize>().GetCurrentCiltureInfo();
            }

            MainPage = new NavigationPage(new MapPage());
        }

        protected override async void OnStart()
        {
            await Task.Yield();

            CheckConnection();
            CheckLatestVersion();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            CheckConnection();
        }

        private async void CheckConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                await MainPage.DisplayAlert("", Resource.NoInternet, Resource.Close);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        private async void CheckLatestVersion()
        {
            var isLatestVersionInstalled = await DependencyService.Get<IVersionComparer>().IsUsingLatestVersion();

            if (isLatestVersionInstalled)
                return;

            var result = await MainPage.DisplayAlert("", Resource.DowloadNewVersion, Resource.Update, Resource.Ignore);

            if (result)
            {
                await DependencyService.Get<IVersionComparer>().OpenAppInStore();
            }
        }
    }
}
