﻿using Mapsui.Geometries;
using Mapsui.Layers;
using Mapsui.Providers;
using Mapsui.Styles;

namespace TransGps.MapLayers
{
    class LocationLayer : AnimatedPointLayer
    {
        private static LocationMemoryProvider _locationMemoryProvider;

        public static LocationLayer CreateLayer()
        {
            _locationMemoryProvider = new LocationMemoryProvider();
            return new LocationLayer(_locationMemoryProvider);
        }

        private LocationLayer(LocationMemoryProvider locationMemoryProvider) : base(locationMemoryProvider)
        {
            Style = new SymbolStyle { Opacity = 0 };
        }

        public Point Location
        {
            set
            {
                _locationMemoryProvider.Location = value;
                UpdateData();
            }
        }
    }
}
