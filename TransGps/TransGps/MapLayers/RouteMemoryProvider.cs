﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;

using Mapsui.Geometries;
using Mapsui.Layers;
using Mapsui.Projection;
using Mapsui.Providers;
using Mapsui.Styles;
using System.Text;
using System.Xml.Linq;

using TransGps.ViewModels;

namespace TransGps.MapLayers
{
    class RouteMemoryProvider : MemoryProvider
    {
        public List<RouteViewModel> SelectedRoutes { get; set; }

        public override IEnumerable<IFeature> GetFeaturesInView(BoundingBox box, double resolution)
        {
            var features = new Features();

            foreach(var route in SelectedRoutes)
            {
                var line = (LineString)Geometry.GeomFromText($"LINESTRING({route.Coordinates})");
                line = new LineString(line.Vertices.Select(s => SphericalMercator.FromLonLat(s.Y, s.X)));
                var feature = new Feature
                {
                    Geometry = line,
                    ["ID"] = $"{route.Id}",
                    ["Label"] = $"Item {route.RouteName}"
                };

                feature.Styles.Add(new VectorStyle { Fill = null, Outline = null, Line = { Color = Color.Blue, Width = 4 } });
                features.Add(feature);
            }

            return features;
        }
    }
}
