﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;

using Mapsui.Geometries;
using Mapsui.Layers;
using Mapsui.Projection;
using Mapsui.Providers;
using Mapsui.Styles;
using System.Text;
using System.Xml.Linq;

using TransGps.Services;
using TransGps.Repositories;
using TransGps.ViewModels;
using Xamarin.Forms.Internals;
using Xamarin.Forms.PlatformConfiguration;

namespace TransGps.MapLayers
{
    class UnitMemoryProvider : MemoryProvider
    {
        private UnitRepository unitRepository = new UnitRepository();
        private RouteRepository routeRepository = new RouteRepository();
        private readonly object _locjObject = new object();

        public List<RouteViewModel> SelectedRoutes { get; set; }

        private NumberFormatInfo _provider;
        private NumberFormatInfo Provider
        {
            get
            {
                if (_provider == null)
                {
                    _provider = new NumberFormatInfo();
                    _provider.NumberDecimalSeparator = ".";
                }

                return _provider;
            }
        }

        private Dictionary<int, RouteViewModel> _dictRoutes;
        private Dictionary<int, RouteViewModel> DictRoutes => _dictRoutes ?? (_dictRoutes = routeRepository.Routes.ToDictionary(r => r.Id, r => r));

        public override IEnumerable<IFeature> GetFeaturesInView(BoundingBox box, double resolution)
        {
            var features = new Features();
            var units = unitRepository.GetUnits(SelectedRoutes);

            lock (_locjObject)
            {
                foreach (var unit in units)
                {
                    var feature = new Feature
                    {
                        Geometry = SphericalMercator.FromLonLat(unit.Longtitude, unit.Lattitude),
                        ["ID"] = $"{unit.Id}",
                        ["Name"] = unit.UnitName,
                        ["Speed"] = $"{unit.Speed}"
                    };

                    var routeName = String.Empty;
                    var strokeColor = String.Empty;

                    if (DictRoutes.TryGetValue(unit.RouteId, out RouteViewModel route))
                    {
                        routeName = route.RouteName;
                        strokeColor = route.RouteColor;
                    }
                    else
                    {
                        routeName = unit.UnitTypeId == 1 ? "A" : unit.UnitTypeId == 2 ? "T" : "";
                        strokeColor = "black";
                    }

                    try
                    {
                        feature.Styles.Add(new SymbolStyle { BitmapId = GetBitmapIdFromStream(routeName, unit.Rotation, strokeColor, "white", unit.UnitTypeId, Provider), SymbolType = SymbolType.Svg }); //, SymbolRotation = step * 10000, SymbolOffset = new Offset(0.5, 0.5, false)
                    }
                    catch (Exception e)
                    {

                        System.Console.WriteLine($"Error: Exception at UnitMemoryProvider.GetFeaturesInView(feature.Styles.Add()) - {e.Message}");
                        if (e.InnerException != null)
                        {
                            System.Console.WriteLine($"InnerException - {e.InnerException.Message}");
                        }
                    }
                    features.Add(feature);


                } 
            }

            return features;
        }

        private int GetBitmapIdFromStream(string routeId, double angle, string strokeColor, string fillColor, int unitTypeId, NumberFormatInfo provider)
        {
            var stream = SvgImageCreator.CreateUnitImage(routeId, angle, strokeColor, fillColor, unitTypeId, provider);

            return BitmapRegistry.Instance.Register(stream);
        }
  
    }
}
