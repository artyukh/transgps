﻿using Mapsui.Providers;
using Mapsui.Geometries;
using System;
using System.Collections.Generic;
using System.Text;
using Mapsui.Projection;
using Mapsui.Styles;
using Mapsui.Utilities;

namespace TransGps.MapLayers
{
    class LocationMemoryProvider : MemoryProvider
    {
        public Point Location { get; set; }

        public override IEnumerable<IFeature> GetFeaturesInView(BoundingBox box, double resolution)
        {
            var features = new Features();

            if (Location != null)
            {
                try
                {
                    var imageStream = EmbeddedResourceLoader.Load("Images.MapPin.png", typeof(LocationLayer));
                    var feature = new Feature
                    {
                        Geometry = Location
                    };

                feature.Styles.Add(new SymbolStyle { BitmapId = BitmapRegistry.Instance.Register(imageStream), SymbolType = SymbolType.Bitmap });
                features.Add(feature); 
                }
                catch (Exception ex)
                {
                    var res = ex.Message;
                    throw;
                }
            }

            return features;
        }
    }
}
