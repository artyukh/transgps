﻿using Mapsui.Layers;
using Mapsui.Providers;
using Mapsui.Styles;

using System.Collections.Generic;
using System.Threading;

using TransGps.ViewModels;

namespace TransGps.MapLayers
{
    class RouteLayer : AnimatedPointLayer
    {
        private static RouteMemoryProvider _routeMemoryProvider;

        public static RouteLayer CreateRouteLayer()
        {
            _routeMemoryProvider = new RouteMemoryProvider();
            
            return new RouteLayer(_routeMemoryProvider);
        }

        private RouteLayer(RouteMemoryProvider routeMemoryProvider) : base(routeMemoryProvider)
        {
            Style = new VectorStyle { Opacity = 0 };
        }
        
        public List<RouteViewModel> SelectedRoutes
        {
            set => _routeMemoryProvider.SelectedRoutes = value;
        }
    }
}
