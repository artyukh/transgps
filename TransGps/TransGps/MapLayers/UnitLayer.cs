﻿using Mapsui.Layers;
using Mapsui.Providers;
using Mapsui.Styles;
using System.Collections.Generic;
using System.Threading;
using TransGps.ViewModels;

namespace TransGps.MapLayers
{
    class UnitLayer : AnimatedPointLayer
    {
        private readonly Timer timer;

        private static UnitMemoryProvider _unitMemoryProvider;

        public static UnitLayer CreateUnitLayer()
        {
            _unitMemoryProvider = new UnitMemoryProvider();

            return new UnitLayer(_unitMemoryProvider);
        }

        private UnitLayer(UnitMemoryProvider unitMemoryProvider) : base(unitMemoryProvider)
        {         
            Style = new VectorStyle { Opacity = 0 };
            timer = new Timer(arg => UpdateData(), this, 0, 5000);
        }

        public List<RouteViewModel> SelectedRoutes
        {
            set => _unitMemoryProvider.SelectedRoutes = value;
        }
    }
}
