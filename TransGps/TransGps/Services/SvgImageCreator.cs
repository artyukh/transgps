﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml.Linq;
using TransGps.Models;

namespace TransGps.Services
{
    static class SvgImageCreator
    {
        public static MemoryStream CreateUnitImage(string number, double rotation, string strokeColor, string fillColor, int unitTypeId, NumberFormatInfo provider)
        {
            #region Автобус зі стрілкою.Коли кут повороту передається. 
            //      < svg version = "1.1" baseProfile = "full"   xmlns = "http://www.w3.org/2000/svg" >

            //     < polygon points = "55 20 45 25 55 0 65 25"
            //              stroke = "black" fill = "white" stroke - width = "2"
            //              transform = "rotate(230,55,40)" />

            //      < circle cx = "55" cy = "40" r = "15" stroke = "black" fill = "white" stroke - width = "2" />
            //      < text x = "55" y = "45" text - anchor = "middle" font - size = "15px" stroke = "black" fill = "black" > 11 </ text >
            //                          </ svg > 
            #endregion

            #region Автобус без стрілки. Коли кут повороту = 0
            //      < svg version = "1.1" baseProfile = "full"   xmlns = "http://www.w3.org/2000/svg" >

            //      < circle cx = "55" cy = "40" r = "15" stroke = "black" fill = "white" stroke - width = "2" />
            //      < text x = "55" y = "45" text - anchor = "middle" font - size = "15px" stroke = "black" fill = "black" > 11 </ text >
            //                                                         </ svg > 
            #endregion

            #region Тролейбус зі стрілкою. Коли кут повороту передається.

            //      < svg version = "1.1" baseProfile = "full"   xmlns = "http://www.w3.org/2000/svg" >


            //      < polygon points = "55 20 45 25 55 0 65 25"
            //              stroke = "black" fill = "white" stroke - width = "2"
            //              transform = "rotate(230,55,40)" />

            //      < polygon points = "48 52 49 52 49 62 48 62"
            //              stroke = "black" fill = "white" stroke - width = "2"
            //              transform = "rotate(230,55,40)" />

            //      < polygon points = "62 52 61 52 61 62 62 62"
            //              stroke = "black" fill = "white" stroke - width = "2"
            //              transform = "rotate(230,55,40)" />

            //      < circle cx = "55" cy = "40" r = "15" stroke = "black" fill = "white" stroke - width = "2" />
            //      < text x = "55" y = "45" text - anchor = "middle" font - size = "15px" stroke = "black" fill = "black" > 11 </ text >
            //                          </ svg > 
            #endregion                         

            #region Тролейбус без стрілки. 
            //      < svg version = "1.1" baseProfile = "full"   xmlns = "http://www.w3.org/2000/svg" >



            //      < polygon points = "48 52 49 52 49 62 48 62"
            //              stroke = "black" fill = "white" stroke - width = "2"
            //              transform = "rotate(230,55,40)" />

            //      < polygon points = "62 52 61 52 61 62 62 62"
            //              stroke = "black" fill = "white" stroke - width = "2"
            //              transform = "rotate(230,55,40)" />

            //      < circle cx = "55" cy = "40" r = "15" stroke = "black" fill = "white" stroke - width = "2" />
            //      < text x = "55" y = "45" text - anchor = "middle" font - size = "15px" stroke = "black" fill = "black" > 11 </ text >
            //                          </ svg > 
            #endregion

            var rotateAngle = rotation.ToString(provider);

            XNamespace xn = ("http://www.w3.org/2000/svg");

            var svgElement = new XElement(xn + "svg");
            var versionAttr = new XAttribute("version", 1.1);
            var xmlnsAttr = new XAttribute("xmlns", "http://www.w3.org/2000/svg");
            var svgWidthAttr = new XAttribute("width", 100);
            var svgHeightAttr = new XAttribute("height", 100);

            svgElement.Add(versionAttr);
            svgElement.Add(xmlnsAttr);
            svgElement.Add(svgWidthAttr);
            svgElement.Add(svgHeightAttr);

            #region Circle
            var circleElement = new XElement("circle");
                var cxAttr = new XAttribute("cx", 55);
                var cyAttr = new XAttribute("cy", 40);
                var rAttr = new XAttribute("r", 15);
                var strokeCircleAttr = new XAttribute("stroke", strokeColor);
                var strokeWidthCircleAttr = new XAttribute("stroke-width", 2);
                var fillAttr = new XAttribute("fill", fillColor);

                circleElement.Add(cxAttr);
                circleElement.Add(cyAttr);
                circleElement.Add(rAttr);
                circleElement.Add(strokeCircleAttr);
                circleElement.Add(strokeWidthCircleAttr);
                circleElement.Add(fillAttr);

                svgElement.Add(circleElement);
            #endregion

            if (unitTypeId == 2) //Тролейбус
            {
                #region Polygon 1
                var polygElement1 = new XElement("polygon");
                var pointsAttr1 = new XAttribute("points", "48 52 49 52 49 62 48 62");
                var polygStrokeAttr1 = new XAttribute("stroke", strokeColor);
                var polygFillAttr1 = new XAttribute("fill", fillColor);
                var polygStrokeWidthAttr1 = new XAttribute("stroke-width", 2);
                var transformAttr1 = new XAttribute("transform", $"rotate({rotateAngle} 55,40)");

                polygElement1.Add(pointsAttr1);
                polygElement1.Add(polygStrokeAttr1);
                polygElement1.Add(polygFillAttr1);
                polygElement1.Add(polygStrokeWidthAttr1);
                polygElement1.Add(transformAttr1);

                svgElement.Add(polygElement1);
                #endregion

                #region Polygon 2
                var polygElement2 = new XElement("polygon");
                var pointsAttr2 = new XAttribute("points", "62 52 61 52 61 62 62 62");
                var polygStrokeAttr2 = new XAttribute("stroke", strokeColor);
                var polygFillAttr2 = new XAttribute("fill", fillColor);
                var polygStrokeWidthAttr2 = new XAttribute("stroke-width", 2);
                var transformAttr2 = new XAttribute("transform", $"rotate({rotateAngle} 55,40)");

                polygElement2.Add(pointsAttr2);
                polygElement2.Add(polygStrokeAttr2);
                polygElement2.Add(polygFillAttr2);
                polygElement2.Add(polygStrokeWidthAttr2);
                polygElement2.Add(transformAttr2);

                svgElement.Add(polygElement2);
                #endregion 
            }

            if (rotation != 0)
            {
                var polygArrow = new XElement("polygon");
                var pointsPolygArrow = new XAttribute("points", "55 20 45 25 55 0 65 25");
                var strokePolygArrow = new XAttribute("stroke", strokeColor);
                var fillPolygArrow = new XAttribute("fill", fillColor);
                var StrokeWidthPolygArrow = new XAttribute("stroke-width", 2);
                var transformPolygArrow = new XAttribute("transform", $"rotate({rotateAngle} 55,40)");

                polygArrow.Add(pointsPolygArrow);
                polygArrow.Add(strokePolygArrow);
                polygArrow.Add(fillPolygArrow);
                polygArrow.Add(StrokeWidthPolygArrow);
                polygArrow.Add(transformPolygArrow);

                svgElement.Add(polygArrow);
            }

            #region Text
            var textElement = new XElement("text");
            var textXAttr = new XAttribute("x", 55);
            var textYAttr = new XAttribute("y", 45);
            var textAnchorAttr = new XAttribute("text-anchor", "middle");
            var textFillAttr = new XAttribute("fill", "black");
            var fontSizeAttr = new XAttribute("font-size", "15px");
            var fontFamilyAttr = new XAttribute("font-family", "serif");
            var fontStyleAttr = new XAttribute("font-weight", "bold");
            var strokeAttr = new XAttribute("stroke", "black");

            textElement.Add(textXAttr);
            textElement.Add(textYAttr);
            textElement.Add(textAnchorAttr);
            textElement.Add(textFillAttr);
            textElement.Add(fontSizeAttr);
            textElement.Add(fontFamilyAttr);
            textElement.Add(fontStyleAttr);
            textElement.Add(strokeAttr);
            textElement.Value = number;
            #endregion

            svgElement.Add(textElement);

            var svgString = svgElement.ToString().Replace(" xmlns=\"\"", "");
            var bytes = Encoding.ASCII.GetBytes(svgString);
            return new MemoryStream(bytes);
        }

        public static MemoryStream CreateMapPinImage()
        {
            #region Map Pin
            //< svg xmlns = "http://www.w3.org/2000/svg" width = "36" height = "56" >
            //      < path d = "M18 .34C8.325.34.5 8.168.5 17.81c0 3.339.962 6.441 2.594 9.094H3l7.82 15.117L18 55.903l7.187-13.895L33 26.903h-.063c1.632-2.653 2.594-5.755 2.594-9.094C35.531 8.169 27.675.34 18 .34zm0 9.438a6.5 6.5 0 1 1 0 13 6.5 6.5 0 0 1 0-13z" fill = "#00b100" />
            //      </ svg > 
            #endregion
            XNamespace xn = ("http://www.w3.org/2000/svg");

            var svgElement = new XElement(xn + "svg");
            //var versionAttr = new XAttribute("version", 1.1);
            var xmlnsAttr = new XAttribute("xmlns", "http://www.w3.org/2000/svg");
            var svgWidthAttr = new XAttribute("width", 36);
            var svgHeightAttr = new XAttribute("height", 56);

            //svgElement.Add(versionAttr);
            svgElement.Add(xmlnsAttr);
            svgElement.Add(svgWidthAttr);
            svgElement.Add(svgHeightAttr);

            var pathElement = new XElement("path");
            var dAttr = new XAttribute("d", "M18 .34C8.325.34.5 8.168.5 17.81c0 3.339.962 6.441 2.594 9.094H3l7.82 15.117L18 55.903l7.187 - 13.895L33 26.903h - .063c1.632 - 2.653 2.594 - 5.755 2.594 - 9.094C35.531 8.169 27.675.34 18 .34zm0 9.438a6.5 6.5 0 1 1 0 13 6.5 6.5 0 0 1 0 - 13z");
            var fillAttr = new XAttribute("fill", "#00b100");

            pathElement.Add(dAttr);
            pathElement.Add(fillAttr);

            svgElement.Add(pathElement);


            var svgString = svgElement.ToString().Replace(" xmlns=\"\"", "");
            var bytes = Encoding.ASCII.GetBytes(svgString);
            return new MemoryStream(bytes);
        }
    }
}
