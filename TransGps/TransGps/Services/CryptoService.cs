﻿using System;
using System.IO;
using System.Xml.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Linq;

namespace TransGps.Services
{
    class CryptoService
    {
        //var root = new XElement("EncryptSource");
        //var routePath = new XElement("RoutePath", cryptoService.GetEncryptedText("http://trans-gps.cv.ua/map/api/get_routes", ));

        //var unitPath = new XElement("UnitPath", cryptoService.GetEncryptedText("http://trans-gps.cv.ua/map/api/get_trackers", this.GetType().MetadataToken));
        //var auth = new XElement("Auth", "");
        //root.Add(routePath);
        //root.Add(unitPath);
        //root.Add(auth);
        //doc.Add(root);
        //using (System.IO.StringWriter writer = new System.IO.StringWriter())
        //{
        //    doc.Save(writer);
        //    var res = writer.ToString();
        //}
        //""
        private byte[] CreateKey(int key, int keyBytes = 32)
        {
            var pass = key.ToString();
            byte[] salt = new byte[] { 80, 70, 60, 50, 40, 30, 20, 10 };
            var keyGenerator = new Rfc2898DeriveBytes(pass, salt, 300);
            return keyGenerator.GetBytes(keyBytes);
        }

        public string GetEncryptedText(string val, int key)
        {
            using (Aes aes = Aes.Create())
            {
                aes.Key = CreateKey(key);
                byte[] encrypted = AesEncryptStringToBytes(val, aes.Key, aes.IV);
                return $"{Convert.ToBase64String(encrypted)};{Convert.ToBase64String(aes.IV)}";
            }
        }

        public string GetDecryptedText(int key, string name)
        {
            var route = GetEncryptedText("http://trans-gps.cv.ua/map/api/get_routes", key);
            var unit = GetEncryptedText("http://trans-gps.cv.ua/map/api/get_trackers", key);
            var assembly = this.GetType().Assembly;
            var stream = assembly.GetManifestResourceStream("TransGps.Encrypt.txt");

            var doc = XDocument.Load(stream);
            var element = doc.Element("EncryptSource").Elements().Where(e => e.Name == name).FirstOrDefault();

            if (element == null) return String.Empty;
            var encryptedText = name == "RoutePath" ? route : unit;//element.Value;

            var encryptedArray = encryptedText.Split(new char[] { ';' });

            if (encryptedArray.Length != 2)
                return String.Empty;

            return AesDecryptStringFromBytes(Convert.FromBase64String(encryptedArray[0]), CreateKey(key), Convert.FromBase64String(encryptedArray[1]));
        }

        private byte[] AesEncryptStringToBytes(string text, byte[] key, byte[] iv)
        {
            if (String.IsNullOrEmpty(text) || (key?.Length ?? 0) == 0 || (iv?.Length ?? 0) == 0)
                return new byte[] { };

            byte[] encrypted;

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (ICryptoTransform encryptor = aes.CreateEncryptor())
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter writer = new StreamWriter(cryptoStream))
                            {
                                writer.Write(text);
                            }
                        }
                    }

                    encrypted = memoryStream.ToArray();
                }
            }

            return encrypted;
        }

        private string AesDecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            if ((cipherText?.Length ?? 0) == 0 || (key?.Length ?? 0) == 0 || (iv?.Length ?? 0) == 0)
            {
                return String.Empty;
            }

            var result = String.Empty;

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                using (MemoryStream memoryStream = new MemoryStream(cipherText))
                {
                    using (ICryptoTransform decryptor = aes.CreateDecryptor())
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader reader = new StreamReader(cryptoStream))
                            {
                                result = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}
