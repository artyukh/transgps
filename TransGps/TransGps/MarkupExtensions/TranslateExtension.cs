﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TransGps.Interfaces;
using System.Resources;
using System.Reflection;

namespace TransGps.MarkupExtensions
{
    [ContentProperty("Text")]
    class TranslateExtension : IMarkupExtension
    {
        readonly CultureInfo cultureInfo;
        const string ResourceId = "TransGps.Resources.Resource";

        public string Text { get; set; }

        public TranslateExtension()
        {
            cultureInfo = DependencyService.Get<ILocalize>().GetCurrentCiltureInfo();
        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
            {
                return String.Empty;
            }

            ResourceManager resourceManager = new ResourceManager(ResourceId, typeof(TranslateExtension).GetTypeInfo().Assembly);

            var translation = resourceManager.GetString(Text, cultureInfo);

            return translation ?? Text;
        }
    }
}
